# Standalone VR Art Exhibition MuseS

This project includes an Unreal Engine project incorporating input from the Muse S BCI in an artistic experience.

## Using this project

### Prerequisites

This project relies on several components and software to function correctly:

1. **Unreal Engine 5.3.2**: Ensure you have Unreal Engine version 5.3.2 installed. You can download it from the [Epic Games Launcher](https://www.unrealengine.com/).
2. **Muse S BCI**: The Muse S brain-computer interface is used to extract blinking and focus levels.
3. **Mind Monitor App**: This app, available for Meta Quest 2 or similar standalone VR headsets, connects to the Muse S via Bluetooth and streams the data as OSC (Open Sound Control).

### Setting Up Mind Monitor App

1. **Connect the Muse S to Mind Monitor**:
   - Open the Mind Monitor app on your Meta Quest 2 or similar standalone VR headset.
   - Pair the app with your Muse S BCI via Bluetooth.

2. **Configure OSC Streaming**:
   - Set the OSC stream destination to `127.0.0.2:8001`. If you need to change this, ensure the corresponding settings are updated in the Unreal Engine project.
   - Adjust the stream settings to send average powers of all channels.

### Installation

1. **Clone the Repository**:
   ```bash
   git clone git@gitlab.com:maria-ivan/standalone-vr-art-exhibition-muses.git
   cd standalone-vr-art-exhibition-muses
   ```

2. **Open the Project in Unreal Engine**:
   - Launch Unreal Engine 5.3.2.
   - Open the project by navigating to the cloned repository folder and selecting the `.uproject` file.

3. **Enable Required Plugins**:
   - Ensure that all necessary plugins for OSC and VR functionality are enabled in Unreal Engine.

### Usage

1. **Run the Mind Monitor App**:
   - Start the OSC stream from the Mind Monitor app on your VR headset.
   
2. **Launch the Unreal Engine Project**:
   - Package the Unreal Engine project for android and deploy it to your VR headset.
   - The project will use the OSC data from the Mind Monitor app to create an artistic VR experience based on blinking and focus levels.

## Features

- Real-time input from Muse S BCI.
- Artistic visualizations based on blinking and focus levels
- Experience includes a burning palace where focus level influences the intensity of the fire, and blinking alternates between day and night.
- Seamless integration with Meta Quest 2 or similar standalone VR headsets.